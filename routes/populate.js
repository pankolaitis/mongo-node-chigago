var express = require('express');
var router = express.Router();
var faker = require('faker');
var _ = require('underscore');
const MongoClient = require('mongodb').MongoClient;
const test = require('assert');
// Connection url
const url = 'mongodb://localhost:27017';
// Database Name
const dbName = 'dms311';
const personalUpvotesLimit = 1000;

const collections = ["abandonedVehicles", "alleyLights", "garbageCarts", "graffitiRemoval", "potHoles", "rodentBaiting",
  "sanitationCode",  "streetLightsAll",  "streetLightsOne", "treeDebris", "treeTrims", "abandonedBuildings"];

router.get('/', (req, res) => {
  var $req = req, $res = res, users = $req.query.users ? parseInt(($req.query.users) || 10000) : 10000,
      docs = $req.query.docs ? parseInt(($req.query.docs) || 100000) : 100000,
      usersList = [], docsLists = [], $userUUID = req.query.user || '', $SRN = req.query.incident || '';
  //if($userUUID) $queryUser.UUID = $userUUID;
  collections.forEach(getData);
  $remaining = collections.length + 1;

  function getData(value, index, array) {
    var $value = value;
    MongoClient.connect(url, function (err, client) {

      const $connection = client.db(dbName);
      if ($SRN) {
        $connection.collection($value).find({"Service Request Number": $SRN}).toArray(function (err, result) {
          if (err) $res.send({error: err});
          if (result.length)
            docsLists.push({"collection_name": $value, docs: result});
          onGetData(client);
        });
      } else {
        $connection.collection($value).aggregate([{$sample: {size: docs}}]).toArray(function (err, result) {
          if (err) $res.send({error: err});
          if (result.length)
            docsLists.push({"collection_name": $value, docs: result});
          onGetData(client);
        });
      }
    });
  }

  getUsers($userUUID);

  function getUsers(userUUID) {
    MongoClient.connect(url, function (err, client) {

      const $connection = client.db(dbName);
      if (userUUID) {
        $connection.collection('citizen').find({"UUID": userUUID}).toArray(function (err, result) {
          if (err) console.log(err);
          usersList = result;
          onGetData(client);
        });
      } else {
        $connection.collection('citizen').aggregate([{$sample: {size: users}}]).toArray(function (err, result) {
          if (err) console.log(err);
          usersList = result;
          onGetData(client);
        });
      }
    });
  }

  function onGetData(client) {
    client.close();
    $remaining--;
    //if(usersList.length > 0 && (docsLists.length == collections.length ) { // same logic but applies regardless results
    if ($remaining === 0) {
      for (var ui = 0; ui < usersList.length; ui++) {

        for (var di = 0; di < docsLists.length; di++) {

          for (var dj = 0; docsLists[di].docs && dj < docsLists[di].docs.length; dj++) {
            var $threshold = $userUUID || $SRN ? -1 : 95;
            if (getRandomInt(100) > $threshold) {
              if (
                  (!usersList[ui].Upvotes ||
                      (usersList[ui].Upvotes.indexOf(docsLists[di].docs[dj]["Service Request Number"]) < 0 && usersList[ui].Upvotes.length < 1000)
                  ) && (
                      !docsLists[di].docs[dj].Upvotes || docsLists[di].docs[dj].Upvotes.indexOf(ui["UUID"]) < 0
                  )
              ) {

                usersList[ui].Upvotes = usersList[ui].Upvotes || [];
                usersList[ui].Upvotes.push(docsLists[di].docs[dj]["Service Request Number"]);
                usersList[ui].UpvotesCount = usersList[ui].UpvotesCount || 0;
                usersList[ui].UpvotesCount++;

                usersList[ui].Wards = usersList[ui].Wards || [];
                usersList[ui].WardsCount = usersList[ui].WardsCount || 0;
                if (docsLists[di].docs[dj]["Ward"] && usersList[ui].Wards.indexOf(docsLists[di].docs[dj]["Ward"]) < 0) {
                  usersList[ui].Wards.push(docsLists[di].docs[dj]["Ward"]);
                  usersList[ui].WardsCount++;
                }

                docsLists[di].docs[dj].Upvotes = docsLists[di].docs[dj].Upvotes || [];
                docsLists[di].docs[dj].Upvotes.push(usersList[ui]["UUID"]);

                docsLists[di].docs[dj].UpvotesCount = docsLists[di].docs[dj].UpvotesCount || 0;
                docsLists[di].docs[dj].UpvotesCount++;


              }
            }
          }
        }
      }
      $remaining = docsLists.length + 1;
      MongoClient.connect(url, function (err, client) {
        if (err) console.log(err);
        var di, dj, ui;
        for (di = 0; di < docsLists.length; di++) {

          for (dj = 0; docsLists[di].docs && dj < docsLists[di].docs.length; dj++) {
            var $docList = docsLists[di];
            var $doc = $docList.docs[dj];
            //if($doc) {
            client.db(dbName).collection($docList.collection_name).updateOne({"_id": $doc._id}, {
              $set: {
                "Upvotes": $doc.Upvotes,
                "UpvotesCount": $doc.UpvotesCount
              }
            }, function () {
              client.close();
              onUpdate();
            });
            //}
          }
        }
      });

      MongoClient.connect(url, function (err, client) {
        var ui;
        const $connection = client.db(dbName);
        for (ui = 0; ui < usersList.length; ui++) {
          if (usersList[ui]) {
            var $user = usersList[ui], userCounter = 0;

            if (err) console.log(err);
            $connection.collection('citizen').updateOne({"_id": $user._id}, {
              $set: {
                "Upvotes": $user.Upvotes,
                "UpvotesCount": $user.UpvotesCount,
                "Wards": $user.Wards,
                "WardsCount": $user.WardsCount,
              }
            }, function () {
              userCounter++;
              client.close();
              if (userCounter == usersList.length) {
                onUpdate();
              }
            });

          }
        }
      });

      function onUpdate() {
        $remaining--;
        if ($remaining === 0) {
          $res.send({
            result: {
              success: "OK",
              Users: usersList,
              Docs: docsLists
            }
          });
        }
      }
    }
  }

});

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
module.exports = router;
