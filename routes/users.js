var express = require('express');
var router = express.Router();
var faker = require('faker');
var _ = require('underscore');
const MongoClient = require('mongodb').MongoClient;
const test = require('assert');
// Connection url
const url = 'mongodb://localhost:27017';
// Database Name
const dbName = 'dms311';

const collections = ["abandonedVehicles", "alleyLights", "garbageCarts", "graffitiRemoval", "potHoles", "rodentBaiting",
  "sanitationCode",  "streetLightsAll",  "streetLightsOne", "treeDebris", "treeTrims", "abandonedBuildings"];

router.get('/insertRandomUsers', (req, res) => {
  var num = req.query.num ? parseInt((req.query.num) || 1) : 1,
      users = [];
  for(var i = 1; i<=num;i++){
    users.push({
      name: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      address: faker.address.streetAddress(),
      createDate: faker.date.past(),
      phone: faker.phone.phoneNumber(),
      UUID: faker.random.uuid()
    });
  }
  onSuccess();
  function onSuccess (data) {
    MongoClient.connect(url, function (err, client) {
      client.db(dbName).collection('citizen').insertMany(users, function (err, result) {
        if(err) res.send({error: err});
        res.send({
          result: {
            UsersCount: users.length,
            Users: users
          }
        });
      });
    });
  }
});


router.get('/getUser', (req, res) => {
  var name = req.query.name ? req.query.name : '',
      last = req.query.last ? req.query.last : '',
      max = req.query.max ? parseInt(req.query.max) || 50 : 50,
      query={};

  if(name)query.name=name;
  if(last)query.lastName=last;

  MongoClient.connect(url, function(err, client) {
    client.db(dbName).collection('citizen').find(query).limit(max).toArray(function(err, result){
      if(err) res.send({error: err});
      res.send({
        result: { result }
      });
    });
  });
});

router.get('/randomUsers', function(req, res, next) {
  var num = req.query.num ? parseInt((req.query.num) || 1) : 1,
      users = [];
  for(var i = 1; i<=num;i++){
    users.push({
      name: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      address: faker.address.streetAddress(),
      createDate: faker.date.past(),
      phone: faker.phone.phoneNumber(),
      UUID: faker.random.uuid()
    });
  }
  res.send({
    result: {
      UsersCount: users.length,
      Users: users
    }
  });
});

module.exports = router;
