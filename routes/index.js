var ObjectId = require('mongodb').ObjectId;


var express = require('express');
var moment = require('moment');
var router = express.Router();
var faker = require('faker');
var _ = require('underscore');
const MongoClient = require('mongodb').MongoClient;
const test = require('assert');
// Connection url
const url = 'mongodb://localhost:27017';
// Database Name
const dbName = 'dms311';

const collections = ["abandonedVehicles", "alleyLights", "garbageCarts", "graffitiRemoval", "potHoles", "rodentBaiting",
  "sanitationCode",  "streetLightsAll",  "streetLightsOne", "treeDebris", "treeTrims", "abandonedBuildings"];
router.get('/randomUsers', function(req, res, next) {
  var num = req.query.num ? parseInt((req.query.num) || 1) : 1,
      users = [];
  for(var i = 1; i<=num;i++){
    users.push({
      name: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      createDate: faker.date.past(),
      UUID: faker.random.uuid()
    });
  }
  res.send({
    result: {
      UsersCount: users.length,
      Users: users
    }
  });
});
//
// // Connect using MongoClient
// MongoClient.connect(url, function(err, client) {
//   // Use the admin database for the operation
//   const adminDb = client.db(dbName).admin();
//
//   // List all the available databases
//   adminDb.listDatabases(function(err, dbs) {
//     test.equal(null, err);
//     test.ok(dbs.databases.length > 0);
//     client.close();
//   });
// });
/* GET home page. */
router.get('/', function (req, res, next) {
  var data = {
    title: 'Express',
    ObjectData: {
      ListTitle: "Ids",
      Items: [
        {"Id": 1}, {"Id": 2}
      ]
    }
  };
  for (var i = 0; i < 1000; i++) {
    var newItem = {};
    newItem["Id"] = i;
    data.ObjectData.Items.push(newItem);
  }
  res.render('index', data);
});

/* GET home page. */
router.get('/kal', function(req, res, next) {
  var data = {
    title: 'Express',
    ObjectData: {
      ListTitle: "MongoResults2",
      Items: []
    }
  };
  MongoClient.connect(url, function(err, client) {
    // Create a collection we want to drop later
    const col = client.db(dbName).collection('collection');

    // Show that duplicate records got dropped
    // let results = col.find({}).then(function(err,results){
    //   for (var i = 0; i<results.length; i++){
    //     var newItem = {};
    //     newItem["Id"]=items[i];
    //     data.ObjectData.Items.push(newItem);
    //   }
    //   res.render('index', data);
    // });
  //   col.find({}).toArray(function(err, items) {
  //     test.equal(null, err);
  //     test.equal(4, items.length);
  //     results = items;
  //     data.ObjectData.Items = items;
  //     client.close();
  //   });
  });
  res.render('index', data);
});
router.get('/clicks', (req, res) => {
  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName);

    col.collection('abandonedVehicles').find({}).skip(100).limit(100).toArray((err, result) => {
      if (err) return console.log(err);
      res.send(result);
    });
  });
});

router.get('/clicks2', (req, res) => {
  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName);

    col.collection('abandonedVehicles').find({"Completion Date":{$gte:"2013"}}).skip(0).limit(100).toArray((err, result) => {
      if (err) return console.log(err);
      res.send({results: result });
    });
  });
});

//queries


router.get('/q1', (req, res) => {
  var start = req.query.start;
  var end = req.query.end;
  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName);
    var counts = [];
    collections.forEach(f1);

    function f1(value, index, array){
      var $value= value;
      const cnt = col.collection(value).count({"Creation Date": {$gte: start}, "Creation Date": {$lte: end}}, function(error, numOfDocs){
        if (error) return console.log(error);
        counts.push({collection_name: $value, totalRequests: numOfDocs});
        if(counts.length == collections.length){
          counts.sort(function(a,b){return parseInt(b.count) - parseInt(a.count)});
          res.send({counts: counts});
        }
      });
    }
  });
});

router.get('/q2', (req, res) => {
  var start = req.query.start;
  var end = req.query.end;
  var cat= req.query.cat.toString();
  var page= req.query.page;
  if(page==null || page < 0){
    page=0;
  }
  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName).collection(cat).aggregate(
        [{$match: {$and: [{"Creation Date": {$gte: new Date(start).toISOString()}}, {"Creation Date": {$lte: new Date(end).toISOString()}}]}}
          ,{
          $group : {
            _id : {year: { $year: {$dateFromString: {dateString: "$Creation Date"}} }, month: { $month: {$dateFromString: {dateString: "$Creation Date"}} }, day: { $dayOfMonth: {$dateFromString: {dateString: "$Creation Date"}} } },
            count: { $sum: 1 }
          }
        }
        ]).sort({_id: 1}).collation({locale: "en_US", numericOrdering: true}).skip(page*100).limit(100).toArray((err, result) => {
      if (err) return console.log(err);
      res.send(result);
    });
  });
});



router.get('/q3', (req, res) => {
  var date1 = new Date(req.query.date);
  var date2= moment(new Date).add(1,'days');

  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName);
    var totalThreads=0;
    var diction = {};
    collections.forEach(f3);

    function f3(value, index, array) {
      var $value = value;
      const cnt = col.collection(value).aggregate(
          [{$match:
                {$and: [{"Creation Date": {$gte: date1.toISOString()}}, {"Creation Date": {$lt: date2.toISOString()}}]}},
            {$group: {_id: "$ZIP Code", count: {$sum: 1}}}
          ]).toArray( (err, result) => {

            var i=0;
            var zip;
            while(result!=null && i<result.length){
              zip = result[i]["_id"].toString();
              diction[zip]= diction[zip] || [];
              result[i]["category"]=$value;
              diction[zip].push(result[i]);
              i=i+1
            }

            totalThreads=totalThreads+1;

            if(totalThreads>11){
              var finaldiction={};
              var key;
              for(key in diction){
                  diction[key].sort(function(a, b){return b.count - a.count});
                  finaldiction[key]=[];
                finaldiction[key].push(diction[key][0]);
                finaldiction[key].push(diction[key][1]);
                finaldiction[key].push(diction[key][2]);

              }

              //res.send({results: result});

              res.send({results: finaldiction});

            }
      });

    }
    });
});



router.get('/q4', (req, res) => {
  var cat= req.query.cat.toString();
  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName).collection(cat)
        .aggregate([{$match:{"Ward": {$ne: ""}}},{$group: {_id: "$Ward", count: { $sum: 1 }}}])
        .sort({count:1})                                            //Do we keep null values?
        .limit(3)
        .toArray((err, result) => {
            if (err) return console.log(err);
            res.send(result);
        });
  });
});



router.get('/q5', (req, res) => {
  var start = req.query.start.toString();
  var end = req.query.end.toString();
  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName);
    var counts = [];
    collections.forEach(f5);


    function f5(value, index, array){
      var $value= value;
      const col = client.db(dbName).collection(value).aggregate([

        {$match: {$and: [{"Creation Date": {$gte: start}}, {"Creation Date": {$lte: end}}, {"Completion Date": {$ne: null}}, {"Completion Date": {$ne: ""}}
        ]}},
        {$project: {"Completion Date": '$Completion Date', "Creation Date": '$Creation Date'}},
        ]).toArray((err, result) =>  {
            if (err) return console.log(err);
            var totalcounts= result.length;
            var j=0;var s=0;
            while(j<totalcounts){
              s=s+(new Date(result[j]["Completion Date"]) - new Date(result[j]["Creation Date"]));
              j=j+1;
            }


            counts.push({"sumTime": s, "counts": totalcounts, "name":$value});

            if(counts.length == collections.length){
              var a=0;
              var b=0;
              var i=0;
              while( i < counts.length){
                a=a+counts[i]["sumTime"];
                b=b+counts[i]["counts"];
                i++;
              }
              var c = 0;
              if(b==0){c=0;}
              else{c=a/b;}
              c=c/1000;
              c=parseInt(c);
              res.send({"Average Completion Time": c+" seconds"});
            }

          }
          );

    }
  });
});

router.get('/q6', (req, res) => {
  var x1 = parseFloat(req.query.x1.toString());
  var y1 = parseFloat(req.query.y1.toString());
  var x2 = parseFloat(req.query.x2.toString());
  var y2 = parseFloat(req.query.y2.toString());
  var date1 = new Date(req.query.date);
  var date2= moment(new Date).add(1,'days');



  MongoClient.connect(url, function(err, client) {
    const col = client.db(dbName);
    var counts = [];
    collections.forEach(f6);

    function f6(value, index, array){
      var $value= value;
      const cnt = col.collection(value).aggregate(

//          {"X Coordinate": {$gte: x2}, "Y Coordinate": {$gte: y2}, "X Coordinate": {$lte: x1}, "Y Coordinate": {$lte: y1}}, function(error, numOfDocs){
          [{$match: {$and:
                  [{"Latitude": {$gte: x2}}, {"Longitude": {$gte: y2}}, {"Latitude": {$lte: x1}}, {"Longitude": {$lte: y1}},
                    {"Creation Date": {$gte: date1.toISOString()}},{"Creation Date": {$lt: date2.toISOString()}}
                  ]}},
            {$group: {_id: null, count: { $sum: 1 }}}
            ]).toArray((err, result) => {
        if (err) return console.log(err);
        var rslt= result[0];
        if(rslt==""|| rslt==null || rslt==[]){
          rslt=0;
        }
        else{
          rslt=result[0].count;
        }
        counts.push({collection_name: $value, counts: rslt});
        if(counts.length == collections.length){
          counts.sort((a,b)=> parseInt(b.counts) - parseInt(a.counts));
          res.send(counts[0]);
        }
      });
    }
  });
});




router.get('/q7', (req, res) => {
  var docLists= [];
  var date1 = new Date(req.query.date);
  var date2 = moment(new Date).add(1,'days');
  collections.forEach(getData);
  function getData(value, index, array) {

    var $value = value;
    MongoClient.connect(url, function (err, client) {
      client.db(dbName).collection($value).find({"Creation Date": {$gte: date1.toISOString()}},{"Creation Date": {$lt: date2.toISOString()}}).sort({"UpvotesCount": -1}).limit(50).toArray(function (err, result) {
        if(err) res.send({error: err});
        docLists.push(result);
        onGetData();
      });
    });
    function onGetData () {
      if(docLists.length == collections.length) {
        res.send({
          result: {
            Docs: _.sortBy(_.flatten(docLists), function(x){ return x.UpvotesCount ? x.UpvotesCount : 0; }).slice(0,50)
          }
        });
      }
    }
  }
});

router.get('/q8', (req, res) => {
    MongoClient.connect(url, function (err, client) {
      client.db(dbName).collection('citizen').find({ }).sort({"UpvotesCount": -1}).limit(50).toArray(function (err, result) {
        if(err) res.send({error: err});
        res.send({
          result: {
            Users: result
          }
        });
      });
    });
});

router.get('/q9', (req, res) => {
    MongoClient.connect(url, function (err, client) {
      client.db(dbName).collection('citizen').find({ }).sort({"WardsCount": -1}).limit(50).toArray(function (err, result) {
        if(err) res.send({error: err});
        res.send({
          result: {
            Users: result
          }
        });
      });
    });
})


router.get('/q10', (req, res) => {
  // var $phone = req.query.phone;
  MongoClient.connect(url, function (err, client) {
    client.db(dbName).collection('citizen').aggregate([{$group : { _id: "$phone" , count: { $sum: 1}, incidents: { $push : "$Upvotes" }}},{$match: {_id: {$ne: null},count: { $gte: 2 }}},{$sort: {total: -1}}]).toArray(function (err, result) {
      if(err) res.send({error: err});
      res.send({
        result: {
          Users: result
        }
      });
    });
  });
});


router.get('/q11', (req, res) => {
  var name = req.query.name ? req.query.name : 'Tomas',
      last = req.query.last ? req.query.last : 'Keeling',
      max = req.query.max ? parseInt(req.query.max) || 50 : 50,
      query={
        "Wards": {$exists: true}
      };

  if(name)query.name=name;
  if(last)query.lastName=last;

  MongoClient.connect(url, function (err, client) {
    client.db(dbName).collection('citizen').find(query).project({"Wards": 1, "_id": -1}).toArray(function (err, result) {
      if(err) res.send({error: err});
      var wards = [];
      for(var i =0;i<result.length;i++){
        wards = _.union(wards, result[i].Wards);
      }
      res.send({
        result: {
          Wards: _.sortBy(wards, function(x){ return x; })
        }
      });
    });
  });
});

router.get('/update', (req, res) => {
  var cat = req.query.cat;
  var _id="";
  if (req.query.hasOwnProperty('_id')){
    _id=req.query._id;
  }



  MongoClient.connect(url, function (err, client) {
    const col = client.db(dbName).collection(cat);
    var jsonObj={};
    var i=0;
    jsonObj=req.query;
    delete jsonObj.cat;

    if(_id!=""){
      delete jsonObj._id;
      col.update({_id: ObjectId(_id)}, {$set: jsonObj});
      res.send("Value with id: '"+_id+"' was updated!");
    }
    else {
      jsonObj["Creation Date"]=new Date().toISOString();
      col.save(jsonObj);
      res.send({message: "The following value was inserted!",object: jsonObj});
    }

  });
});



 module.exports = router;

